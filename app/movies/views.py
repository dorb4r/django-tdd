from django.http import Http404
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from movies.models import Movie
from movies.serializers import MovieSerializer


class MoviesViewSet(ViewSet):
    """

    """

    model = Movie
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

    def get_object(self, pk):
        """
        Get the Object from the DB
        """
        try:
            return self.model.objects.get(pk=pk)
        except Movie.DoesNotExist:
            raise Http404

    def list(self, request):
        serializer = self.serializer_class(self.queryset.all(), many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "title": openapi.Schema(type=openapi.TYPE_STRING),
                "genre": openapi.Schema(type=openapi.TYPE_STRING),
                "year": openapi.Schema(type=openapi.TYPE_STRING),
            },
        )
    )
    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except ValidationError as e:
            raise e

    def retrieve(self, request, pk):
        movie = self.get_object(pk)
        serializer = self.serializer_class(movie)
        return Response(serializer.data)

    def destroy(self, request, pk):
        movie = self.get_object(pk)
        movie.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "title": openapi.Schema(type=openapi.TYPE_STRING),
                "genre": openapi.Schema(type=openapi.TYPE_STRING),
                "year": openapi.Schema(type=openapi.TYPE_STRING),
            },
        )
    )
    def update(self, request, pk):
        movie = self.get_object(pk)
        serializer = self.serializer_class(movie, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

import pytest

from movies.models import Movie


@pytest.mark.django_db
def test_add_movie(client):
    """
    Sanity add movie test
    """
    movies = Movie.objects.all()
    assert len(movies) == 0

    resp = client.post(
        f"/api/movies/",
        {"title": "The Big Lebowski", "genre": "comedy", "year": "1998"},
        content_type="application/json",
    )
    assert resp.status_code == 201
    assert resp.data["title"] == "The Big Lebowski"

    movies = Movie.objects.all()
    assert len(movies) == 1


@pytest.mark.django_db
def test_add_movie_invalid_json(client):
    """
    Empty POST data request
    """
    movies = Movie.objects.all()
    assert len(movies) == 0

    resp = client.post(f"/api/movies/", {}, content_type="application/json")
    assert resp.status_code == 400

    movies = Movie.objects.all()
    assert len(movies) == 0


@pytest.mark.django_db
def test_add_movie_invalid_json_keys(client):
    """
    Data is not complete
    """
    movies = Movie.objects.all()
    assert len(movies) == 0

    resp = client.post(
        f"/api/movies/",
        {"title": "The Big Lebowski", "genre": "comedy"},
        content_type="application/json",
    )
    assert resp.status_code == 400

    movies = Movie.objects.all()
    assert len(movies) == 0


@pytest.mark.django_db
def test_get_single_movie(client, add_movie):
    """
    Get single existing Movie
    """
    # Create a Movie in the DB
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    # Request the Movie using the API
    resp = client.get(f"/api/movies/{movie.id}/")

    # Assert the response
    assert resp.status_code == 200
    assert resp.data["title"] == "The Big Lebowski"


@pytest.mark.django_db
def test_get_single_movie_incorrect_id(client):
    """
    Try to get none-existing movie from DB
    """
    resp = client.get(f"/api/movies/{30}/")
    assert resp.status_code == 404


@pytest.mark.django_db
def test_get_all_movies(client, add_movie):
    movie_one = add_movie(title="The Big Lebowski", genre="comedy", year="1998")
    movie_two = add_movie("No Country for Old Men", "thriller", "2007")
    resp = client.get(f"/api/movies/")
    assert resp.status_code == 200
    assert resp.data[0]["title"] == movie_one.title
    assert resp.data[1]["title"] == movie_two.title


@pytest.mark.django_db
def test_delete_a_movie(client, add_movie):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    get_resp = client.get(f"/api/movies/{movie.id}/")
    assert get_resp.status_code == 200

    delete_resp = client.delete(f"/api/movies/{get_resp.data['id']}/")
    assert delete_resp.status_code == 204

    list_resp = client.get(f"/api/movies/")
    assert list_resp.status_code == 200
    assert len(list_resp.data) == 0


@pytest.mark.django_db
def test_delete_incorrect_movie_id(client):
    response = client.delete(f"/api/movies/30/")
    assert response.status_code == 404


@pytest.mark.django_db
def test_modify_movie(client, add_movie):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    update_response = client.put(
        f"/api/movies/{movie.id}/",
        {"title": "The Big Lebowski The second", "genre": "comedy", "year": "1990"},
        content_type="application/json",
    )
    assert update_response.status_code == 200
    assert update_response.data["title"] == "The Big Lebowski The second"
    assert update_response.data["year"] == "1990"

    retrieve_response = client.get(f"/api/movies/{movie.id}/")

    assert retrieve_response.status_code == 200
    assert retrieve_response.data["title"] == "The Big Lebowski The second"
    assert retrieve_response.data["year"] == "1990"


@pytest.mark.django_db
def test_modify_incorrect_movie_is(client):
    update_response = client.put(
        f"/api/movies/30/",
        {"title": "The Big Lebowski The second", "genre": "comedy", "year": "1990"},
        content_type="application/json",
    )
    assert update_response.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize(
    "add_movie, payload, status_code",
    [
        ["add_movie", {}, 400],
        ["add_movie", {"title": "The Big Lebowski", "genre": "comedy"}, 400],
    ],
    indirect=["add_movie"],
)
def test_modify_movie_invalid_json(client, add_movie, payload, status_code):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    update_response = client.put(
        f"/api/movies/{movie.id}/", {}, content_type="application/json"
    )
    assert update_response.status_code == 400
